## Tripster

<p>Programar uma viagem é sempre uma tarefa árdua: descobrir locais para visitar, conseguir informações sobre atrações temporárias, pegar indicações de bares e restaurantes perto de onde estará hospedado... Mas agora essas informações estarão reunidas em um único portal.</p>
<p>O objetivo do sistema é otimizar o planejamento de viagens, fazendo sugestões com base nas preferências de usuário, para construir o melhor roteiro para qualquer tipo de  viagem.</p>
<p>O usuário informa os detalhes de sua viagem, como itinerário e datas, bem como o perfil da viagem, como turismo, viagem em família ou a trabalho. Com essas informações  fornecidas, o sistema faz uma ordenação dos eventos e exibe para que o usuário analise e selecione o que mais o interesse. Assim o sistema terá um roteiro estruturado para  aquela viagem.</p>
<p>Dessa forma, haverá um ambiente amistoso que reúne todos os eventos elencados de acordo com as preferências de cada usuário, para que a viagem seja ainda mais  proveitosa.</p>
> Sua viagem é o nosso caminho!
> Time Tripstr

