Rails.application.routes.draw do
  devise_for :users, :controllers => { registrations: 'registrations' }

  resources :users, only: [] do
    collection do
      get 'dashboard'
      get 'show'
    end
  end

  resources :trips do
    resources :places, only: [ :show, :edit, :update ] do
      member do
        post 'save_events'
      end
    end
  end

  root to: 'users#show'
end
