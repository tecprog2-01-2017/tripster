require 'rubygems'
require 'eventful/api'

module EventfulApi
  @api_key = 'cg4KVf69j2g2fKLg'

  def self.search(location, arrival, departure)
    eventful = Eventful::API.new @api_key
    date_range = sprintf("%04d%02d%02d00-%04d%02d%02d00",
                         arrival.year, arrival.month, arrival.day,
                         departure.year, departure.month, departure.day)
    _e = eventful.call 'events/search', location: location, date: date_range, sort_order: 'date', page_size: 250
    if _e["events"].nil?
      return []
    end
    return _e["events"]["event"]
  end

  def self.get_event(id)
    eventful = Eventful::API.new @api_key
    eventful.call 'events/get', id: id 
  end

  def self.retrieve(tokens)
    if tokens == nil || tokens.length < 1 then return nil end
    eventful = Eventful::API.new @api_key
    events = []
    tokens.each do |tk|
      events.push(eventful.call 'events/get', :id => tk)
    end
    return events
  end
end
