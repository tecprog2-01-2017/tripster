class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.belongs_to :trip, index: true
      t.string :eventful_id
      t.timestamps
    end
  end
end
