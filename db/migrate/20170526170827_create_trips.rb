class CreateTrips < ActiveRecord::Migration[5.0]
  def change
    create_table :trips do |t|
      t.string :name
      t.string :type
      t.references :user

      t.timestamps
    end
  end
end
