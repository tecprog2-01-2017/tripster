class AddAttributesToPlaces < ActiveRecord::Migration[5.0]
  def change
    add_column :places, :location, :string
    add_column :places, :arrival, :datetime
    add_column :places, :departure, :datetime
  end
end
