class AddArriveAndDepartureToTrip < ActiveRecord::Migration[5.0]
  def up
    add_column :trips, :arrive, :datetime
    add_column :trips, :departure, :datetime
  end
  def down
    remove_column :trips, :arrive
    remove_column :trips, :departure
  end
end
