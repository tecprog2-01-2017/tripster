class TripsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_trip, only: [:show, :edit, :update, :destroy]

  # GET /trips?select=:select
  # GET /trips?select=:select.json
  def index
    if (!params.has_key?(:select))
      redirect_to trips_path(select: 'current')
    end
    if (params[:select] == 'previous')
      @trips = current_user.previous_trips
      @title = 'Últimas viagens'
    else
      @trips = current_user.current_trips
      @title = 'Minhas viagens'
    end
  end

  # GET /trips/:id
  # GET /trips/:id.json
  def show
    @places = @trip.places.sort { |a, b| a.arrival <=> b.arrival }
    @plan = @trip.events.map { |e| EventfulApi.get_event(e.eventful_id) }
    Rails.logger.debug @plan[1]
  end

  # GET /trips/new
  # GET /trips/new.json
  def new
    @trip = Trip.new
    @trip.places.build
  end

  # GET /trips/:id/edit
  def edit
  end

  # POST /trips
  # POST /trips.json
  def create
    @trip = Trip.new(trip_params)
    @trip.user = current_user

    @trip.arrive = @trip.places.min {|a, b| a.arrival <=> b.arrival }.arrival
    @trip.departure = @trip.places.max { |a, b| a.departure <=> b.departure }.departure


    respond_to do |format|
      if @trip.save
        format.html { redirect_to @trip, notice: 'Sua viagem foi criada.' }
        format.json { render :show, status: :created, location: @trip }
      else
        format.html { render :new }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trips/:id
  # PATCH/PUT /trips/:id.json
  def update
    respond_to do |format|
      if @trip.update(trip_params)
        format.html { redirect_to @trip, notice: 'Viagem editada.' }
        format.json { render :show, status: :ok, location: @trip }
      else
        format.html { render :edit }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trips/:id
  # DELETE /trips/:id.json
  def destroy
    @trip.destroy
    respond_to do |format|
      format.html { redirect_to trips_url, notice: 'Viagem removida.' }
      format.json { head :no_content }
    end
  end

  private
  def trip_params
    params.require(:trip).permit(:name, :arrive, :departure,
                                 places_attributes: [:id, :location, :arrival, :departure, :_destroy])
  end

  def set_trip
    @trip = Trip.find_by(id: params[:id])

    if @trip.nil?
      render_404
    elsif @trip.user_id != current_user.id
      render_402
    end
  end
end
