class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  def render_402
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/402", :layout => false, :status => :forbbiden }
      format.xml  { head :forbbiden }
      format.any  { head :forbbiden }
    end
  end

  def render_404
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end
end
