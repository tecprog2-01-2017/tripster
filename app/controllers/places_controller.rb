require 'eventful_api'

class PlacesController < ApplicationController
  before_action :set_place, only: [:show, :edit, :update, :destroy, :save_events]
  helper TripsHelper

  # GET /places
  # GET /places.json
  def index
    @places = Place.all
  end

  # GET /places/1
  # GET /places/1.json
  def show
    @events = EventfulApi.search(@place.location, @place.arrival, @place.departure)
    @current_events = @trip.events.map { |e| e.eventful_id }
  end

  # GET /places/new
  def new
    @place = Place.new
  end

  # GET /places/1/edit
  def edit
  end

  # POST /places
  # POST /places.json
  def create
    @place = Place.new(place_params)
    @place.trip_id = params[:trip_id]

    respond_to do |format|
      if @place.save
        format.html { redirect_to edit_trip_path(@place.trip), notice: 'Lugar criado com sucesso.' }
        format.json { render :show, status: :created, location: @place }
      else
        format.html { render :new }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /places/1
  # PATCH/PUT /places/1.json
  def update
    respond_to do |format|
      if @place.update(place_params)
        format.html { redirect_to trip_place_path(@trip, @place), notice: 'Lugar atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @place }
      else
        format.html { render :edit }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /places/1
  # DELETE /places/1.json
  def destroy
    @place.destroy
    respond_to do |format|
      format.html { redirect_to places_url, notice: 'Lugar removido com sucesso.' }
      format.json { head :no_content }
    end
  end

  # POST /places/1/save_events
  def save_events
    if params.has_key? :events or params.has_key? :last_events
      respond_to do |format|
        if params.has_key? :events
          params[:events].each do |e_id| 
            if @trip.events.where(eventful_id: e_id).empty? 
              Event.create(trip: @trip, eventful_id: e_id)
            end
          end
        end
        if params.has_key? :last_events
          params[:last_events].each do |e_id, was_included|
            event = Event.where(eventful_id: e_id).first
            if event and was_included and not (params.has_key? :events and params[:events].include? e_id)
              event.destroy
            end
          end
        end

        format.html { redirect_to trip_path(@trip), notice: 'Plano de viagem atualizado!' }
        format.json { render :show, status: :ok, location: @trip }
      end
    else
      redirect_to trip_place_path(@trip, @place)
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_place
    @trip = Trip.find_by(id: params[:trip_id])
    @place = Place.find_by(id: params[:id])
    if not @trip or not @place
      render_402
    elsif @trip.user_id !=  current_user.id
      render_404
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def place_params
    params.require(:place).permit(:location, :arrival, :departure)
  end
end
