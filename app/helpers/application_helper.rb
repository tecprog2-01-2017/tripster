module ApplicationHelper
  def date_format(date)
    date.day.to_s + "/" + date.month.to_s + "/" + date.year.to_s
  end

  def link_to_add_places(name, f, association, html_class="")
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(partial: 'places/field', locals: { places_form: builder })
    end
    link_to(name, '#', class: html_class + " add_places", data: {id: id, fields: fields.gsub("\n", "")})
  end

end
