module TripsHelper
  def format_time(str)
    return DateTime.parse(str).strftime("%d/%m/%y %H:%M:%S")
  end
end
