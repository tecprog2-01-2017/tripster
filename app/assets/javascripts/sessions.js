$(document).on('sessions#new:loaded', function () {
    var img_array = [1, 2, 3],
        newIndex = 0,
        index = 0,
        interval = 5000;
    (function changeBg() {
        index = (index + 1) % img_array.length;

        $('body.sessions_body.action_new').css('backgroundImage', function () {
            $('#fullPage').animate({
                backgroundColor: 'transparent'
            }, 1000, function () {
                setTimeout(function () {
                    $('#fullPage').animate({
                        backgroundColor: 'rgb(255,255,255)'
                    }, 1000);
                }, 3000);
            });
            return 'url("/assets/tripster_0' + img_array[index] + '.jpeg")';
        });
        setTimeout(changeBg, interval);
    })();
});

$(document).on("page:change", function(){
  var data = $('body').data();
  $(document).trigger(data.controller + ':loaded');
  $(document).trigger(data.controller + '#' + data.action + ':loaded');
});

$(document).ready(function () {
    $(document).trigger('page:change');
});

