class Event < ApplicationRecord
  belongs_to :trip

  validates :eventful_id, presence: true
end
