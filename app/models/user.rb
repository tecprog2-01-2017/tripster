class User < ApplicationRecord
  has_many :trips, dependent: :destroy
  has_many :previous_trips, -> { where("departure < ?", DateTime.now).order(departure: :asc) }, :class_name => "Trip", :source => :trips
  has_many :current_trips, -> { where("departure >= ?", DateTime.now).order(departure: :asc) }, :class_name => "Trip", :source => :trips
  
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Validation
  #validates_confirmation_of :password
  validates :first_name, :last_name, presence: true
  validates_format_of :first_name, :last_name, :with => /\A([A-Z]*[a-z]+[\s]*)+\z/

end
