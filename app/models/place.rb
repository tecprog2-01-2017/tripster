class Place < ApplicationRecord
	serialize :events, Array

  belongs_to :trip, required: false

  validates :location, presence: true
  validates :arrival, presence: true
  validates :departure, presence: true
  validate  :departure_after_arrival

  def arrival_date_format
    self.arrival.day.to_s + "/" + self.arrival.month.to_s + "/" + self.arrival.year.to_s
  end

  def departure_date_format
    self.departure.day.to_s + "/" + self.departure.month.to_s + "/" + self.departure.year.to_s
  end

  private
  def departure_after_arrival
    return if departure.blank? || arrival.blank?

    errors.add(:departure, "cannot be before the arrival")  if departure < arrival
  end

end
