class Trip < ApplicationRecord
  belongs_to :user
  has_many :places
  has_many :events
  accepts_nested_attributes_for :places,
    :allow_destroy => true,
    :reject_if     => :all_blank

  validates :name, presence: true
  validates :arrive, presence: true
  validates :departure, presence: true
  validate  :departure_after_arrive

  private
  def departure_after_arrive
    return if departure.blank? || arrive.blank?

    errors.add(:departure, "cannot be before the arrive")  if departure < arrive
  end

end
