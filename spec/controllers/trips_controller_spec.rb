require 'rails_helper'

RSpec.describe TripsController, type: :controller do
  before(:each) do
    sign_in FactoryGirl.create(:user)
  end

  def get_params(trip, place) 
    params = FactoryGirl.attributes_for(trip)
    params[:places_attributes] = FactoryGirl.attributes_for(place)
    params[:places_attributes][:id] = nil
    return params
  end

  describe 'GET #index' do
    it 'renders the view :index'  do 
      get :index, params: { select: 'current' }
      expect(response).to render_template("index")
      get :index, params: { select: 'previous' }
      expect(response).to render_template("index")
    end
  end

  describe 'GET #new' do
    it 'renders the view :new' do 
      get :new
      expect(response).to render_template("new")
    end
  end

  describe 'GET #edit' do
    it 'renders the view :edit' do 
      post :create, params: { trip: get_params(:trip, :place) }
      get :edit, params: { id: Trip.last }
      expect(response).to render_template("edit")
    end
  end

  describe 'GET #show' do
    it 'renders the view :show' do 
      post :create, params: { trip: get_params(:trip, :place) }
      get :show, params: { id: Trip.last }
      expect(response).to render_template("show")
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      it 'creates new trip' do 
        expect{
          post :create, params: { trip: get_params(:trip, :place) }
        }.to change(Trip, :count).by(1)
      end

      it 'redirects to the new trip' do
        post :create, params: { trip: get_params(:trip, :place) }
        expect(response).to redirect_to Trip.last 
      end 
    end

    context 'with invalid attributes' do
      it 'does not save new trip' do
        expect{
          post :create, params: { trip: get_params(:trip_no_name, :place) }
        }.to_not change(Trip, :count)
      end

      it 're-renders the new method' do
        post :create, params: { trip: get_params(:trip_no_name, :place) }
        expect(response).to render_template("new")
      end
    end

    context 'with invalid places attributes' do
      it 'does not save new trip' do
        expect{
          post :create, params: { trip: get_params(:trip, :place_no_location) }
        }.to_not change(Trip, :count)
      end

      it 're-renders the new method' do
        post :create, params: { trip: get_params(:trip, :place_no_location) }
        expect(response).to render_template("new")
      end
    end

  end

  describe 'POST #update' do
    context "with valid attributes"  do
      before :each do
        post :create, params: { trip: get_params(:trip, :place) }
        @trip = Trip.last
        @trip_edited = get_params(:trip_edited, :place) 
      end

      it "changes @trip attributes" do
        put :update, params: { id: @trip, trip: @trip_edited}
        @trip.reload
        expect(@trip.name).to eq(@trip_edited[:name])
      end

      it "redirects to the updated trip" do
        put :update, params: { id: @trip, trip: @trip_edited}

        expect(response).to redirect_to @trip
      end
    end

    context "with invalid attributes" do
      before :each do
        post :create, params: { trip: get_params(:trip, :place) }
        @trip = Trip.last
        @trip_edited = get_params(:trip_no_name, :place)
      end

      it "does not change @trip's attributes"  do
        put :update, params: { id: @trip, trip: @trip_edited }

        @trip.reload
        expect(@trip.name).to_not eq(@trip_edited[:name])
      end

      it "re-renders the edit method" do
        put :update, params: { id: @trip, trip: @trip_edited}        
        expect(response).to render_template("edit")
      end
    end
  end

  describe 'DELETE #delete' do
    before :each do
      post :create, params: { trip: get_params(:trip, :place) }
      @trip = Trip.last
    end

    it "deletes the trip" do
      expect{
        delete :destroy, params: { id: @trip }      
      }.to change(Trip,:count).by(-1)
    end

    it "redirects to trips#index" do
      delete :destroy, params: { id: @trip }
      expect(response).to redirect_to(trips_path)
    end
  end

end
