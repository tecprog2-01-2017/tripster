require 'rails_helper'
require 'support/controller_helpers'

RSpec.describe UsersController, type: :controller do

  describe "GET #show" do
    it "renders the show template" do
      sign_in FactoryGirl.create(:user)
      get :show
      expect(response).to render_template("show")
    end
  end

end
