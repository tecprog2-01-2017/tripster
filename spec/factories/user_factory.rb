FactoryGirl.define do 
  factory :user do
    email "123@email.com"
    password "123456"
    first_name "John"
    last_name "Doe"
  end

  factory :user_no_name, class: User do
    email "123@email.com"
    password "123456"
    first_name ""
    last_name ""
  end

 factory :user_nil_name, class: User do
    email "123@email.com"
    password "123456"
    first_name nil
    last_name nil
  end

 factory :user_invalid_name, class: User do
    email "123@email.com"
    password "123456"
    first_name "123"
    last_name "$nome%"
  end

end

