FactoryGirl.define do
  factory :event do
    eventful_id "event_id"
  end

  factory :event_invalid, class: Event do
    eventful_id nil
  end
end
