FactoryGirl.define do 
  factory :trip do
    name 'Trip'
    arrive Time.now
    departure Time.now.advance(days: +3)
  end

  factory :trip_numbers, class: Trip do
    name 'name 11'
    arrive Time.now
    departure Time.now.advance(days: +3)
  end

  factory :trip_no_name, class: Trip do
    name ''
    arrive Time.now
    departure Time.now.advance(days: +3)
  end

  factory :trip_nil_name, class: Trip do
    name nil
    arrive Time.now
    departure Time.now.advance(days: +3)
  end

  factory :trip_edited, class: Trip do
    name 'Edited'
    arrive Time.now
    departure Time.now.advance(days: +3)
  end

  factory :trip_no_arrive, class: Trip do
    name 'Trip'
    arrive nil
    departure Time.now
  end

  factory :trip_no_departure, class: Trip do
    name 'Trip'
    arrive Time.now
    departure nil
  end

  factory :trip_invalid_date, class: Trip do
    name 'Trip'
    arrive Time.now
    departure Time.now.advance(days: -1)
  end

end

