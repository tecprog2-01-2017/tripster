FactoryGirl.define do
  factory :place do
    location 'Sao Paulo'
    arrival Time.now
    departure Time.now.advance(days: 20)
  end

  factory :place_edited, class: Place do
    location 'Rio de Janeiro'
    arrive Time.now  
    departure Time.now.advance(days: 20)
  end

  factory :place_no_location, class: Place do
    location ''
    arrival Time.now
    departure Time.now.advance(days: 20)
  end

  factory :place_nil_location, class: Place do
    location nil
    arrival Time.now
    departure Time.now.advance(days: 20)
  end

  factory :place_no_arrival, class: Place do
    location 'Brasília'
    arrival nil
    departure Time.now.advance(days: 20)
  end

  factory :place_no_departure, class: Place do
    location 'Brasília'
    arrival Time.now
    departure nil
  end

 factory :place_invalid_date, class: Place do
    location 'Sao Paulo'
    arrival Time.now  
    departure Time.now.advance(days: -20)
  end

end
