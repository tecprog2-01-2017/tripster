require 'rails_helper'

RSpec.describe Place, type: :model do
  before do
    @user = FactoryGirl.create(:user)
    params = FactoryGirl.attributes_for(:trip)
    params[:user_id] = @user.id
    @trip = Trip.create(params)
  end

  def get_params(sym) 
    params = FactoryGirl.attributes_for(sym)
    params[:trip_id] = @trip.id
    return params
  end

  it 'is valid with valid attributes' do
    place = Place.new(get_params :place)
    expect(place.save).to be_truthy
  end

  it 'is not valid with empty location' do
    place = Place.new(get_params :place_no_location)
    expect(place.save).to be_falsey
  end

  it 'is not valid without a location' do
    place = Place.new(get_params :place_nil_location)
    expect(place.save).to be_falsey
  end

  it 'is not valid without an arrival' do
    place = Place.new(get_params :place_no_arrival)
    expect(place.save).to be_falsey
  end

  it 'is not valid without a departure' do
    place = Place.new(get_params :place_no_departure)
    expect(place.save).to be_falsey
  end

  it 'is not valid with departure before arrival' do
    place = Place.new(get_params :place_invalid_date)
    expect(place.save).to be_falsey
  end
     
end
