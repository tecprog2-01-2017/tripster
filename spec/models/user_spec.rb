require 'rails_helper'

RSpec.describe User, type: :model do
  context 'Validacao' do

    it 'is valid with valid name' do 
      expect(FactoryGirl.build(:user).save).to be_truthy
    end
    
    it 'is not valid with empty name' do
      expect(FactoryGirl.build(:user_no_name)
.save).to be_falsey
    end

    it 'is not valid without a name' do
      expect(FactoryGirl.build(:user_nil_name)
.save).to be_falsey
    end

    it 'is not valid with invalid name' do 
      expect(FactoryGirl.build(:user_invalid_name).save).to be_falsey
    end
  end
  
end
