require 'rails_helper'

RSpec.describe Event, type: :model do
  before do
    @user = FactoryGirl.create(:user)
    @trip = Trip.create(name: "Trip", arrive: Time.now, departure: Time.now, user: @user)
  end

  def get_params(sym) 
    params = FactoryGirl.attributes_for(sym)
    params[:trip_id] =  @trip.id
    return params
  end

  context 'validation' do 
    it 'is valid with valid eventful_id: "event_id"' do
      event = Event.new(get_params :event)
      expect(event.save).to be_truthy
    end

    it 'is not valid withou an eventful_id' do
      event = Event.new(get_params :event_invalid)
      expect(event.save).to be_falsey
    end
  end
end
