require 'rails_helper'

RSpec.describe Trip, type: :model do
  before do
    @user = FactoryGirl.create(:user)
  end

  def get_params(sym) 
    params = FactoryGirl.attributes_for(sym)
    params[:user_id] =  @user.id
    return params
  end

  context 'validation' do 
    it 'is valid with valid name: "Trip"' do
      trip = Trip.new(get_params :trip)
      expect(trip.save).to be_truthy
    end

    it 'is valid with valid name: "name 11"' do
      trip = Trip.new(get_params(:trip_numbers))
      expect(trip.save).to be_truthy
    end

    it 'is not valid with empty name' do 
      trip = Trip.new(get_params(:trip_no_name))
      expect(trip.save).to be_falsey
    end

    it 'is not valid without a name' do 
      trip = Trip.new(get_params(:trip_nil_name))
      expect(trip.save).to be_falsey
    end
    
    it 'is not valid without an arrive date' do
      trip = Trip.new(get_params(:trip_no_arrive))
      expect(trip.save).to be_falsey
    end

    it 'is not valid without a departure date' do
      trip = Trip.new(get_params(:trip_no_departure))
      expect(trip.save).to be_falsey
    end

  end
end
